# README

This repository contains "CloudForms and Ansible Deep Dive" lab presented during Red Hat Tech Exchange FY20.

View best rendering experience, use [GitLab Pages URL](https://redhat-cop.gitlab.io/cloudforms-ansible-integration/rhte-fy20/).

Please let us know about bugs and glitches by using the [GitLab Issue Tracker](https://gitlab.com/redhat-cop/cloudforms-ansible-integration/rhte-fy20/issues).
