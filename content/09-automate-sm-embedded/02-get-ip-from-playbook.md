+++
title = "Get IP Address from IPAM"
weight = 2
+++

### Run an Ansible Playbook during VM provisioning

With the integration of Ansible into CloudForms, we want to make it easier for customers and partners to modify out of the box behavior and make it easy to integrate with third party solutions.

In this part of the lab, we want to run an Ansible Playbook during VM provisioning to show how to modify the State Machine and how we can use Ansible Playbooks.

### Create a new Automate Domain

Since the Automate Domains shipped with CloudForms are read only, we have to create our own Domain first.

- Navigate to ***Automation*** -> ***Automate***

- Click on ***Configuration*** -> ***Add a new Domain***

- Enter the following details:

***Name:*** Lab

***Description:*** Lab Domain

***Enabled:*** Check

- Click ***Add***

### Copy VM Provisioning State Machine

To be able to make changes to the State Machine, we have to copy it to our writeable Domain first.

- Navigate to the VM Provisioning State Machine:

    ***ManageIQ*** -> ***Infrastructure*** -> ***VM*** -> ***Provisioning*** -> ***StateMachines*** -> ***VMProvision_VM*** -> ***Provision VM from Template (template)***

- Click on ***Configuration*** -> ***Copy this Instance***

{{% notice warning %}}
Make sure you highlight the "Provision VM from Template (template)" instance when initiating the copy!
{{% /notice %}}

- Accept the defaults when confirming the copy

- Click ***Copy*** to confirm

- After the copy was completed, you should see a confirmation page

### Copy the Methods Class

To be able to create a new Method and Instance, we have to copy the "Methods" Class as well.

- Navigate to the VM Provisioning State Machine Methods Class:

    ***ManageIQ*** -> ***Infrastructure*** -> ***VM*** -> ***Provisioning*** -> ***StateMachines*** -> ***Methods***

- Click on ***Configuration*** -> ***Copy this Class***

- Accept the defaults when confirming the copy

- Click ***Copy*** to confirm

### Create the Ansible Playbook Method

Starting with CloudForms 4.6 we can create Methods of type "Playbook" which, instead of running Ruby code, execute an Ansible Playbook.

- Click on the ***Methods*** Class and then the ***Methods*** tab the right part of the window

- Create a new Method to run an Ansible Playbook. Click on ***Configuration*** -> ***Add a new Method***

- Switch the Method Type to "Playbook"

- Use the following details to fill out the form.

***Name:*** phpipam_next_ip

***Display Name:*** get next free IP from phpIPAM

***Repository:*** GitLab

***Playbook:*** playbooks/phpipam-next-ip.yaml

***Machine Credentials:*** Virtual Machine Credentials

***Hosts:*** localhost

***Max TTL (mins):*** 30

***Logging Output:*** On Error

- Click ***Add*** to create the Ansible Playbook Method

### Create the Ansible Playbook Instance

To be able to call the Method from a StateMachine, we need an associated Instance.

- Click on the ***Instances*** tab and ***Configuration*** -> ***Add a new Instance***

- Enter the following details into the Dialog:

***Name:*** phpipam_next_ip

***Display Name:*** get next free IP from PHPIPAM

***Fields:*** In the table search the row "execute" and put "phpipam_next_ip" into the "value" field

- Click ***Add*** to save the Instance

### Modify the State Machine

To run the Ansible Playbook during Virtual Machine Provisioning, we have to change the "AcquireIPAddress" state to the State Machine. It would probably be cleaner to modify the Schema and add a new state, but for the purpose of the lab we want to keep things simple and use an existing placeholder.

- Click on the "Provision VM from Template" Instance

- Click on ***Configuration*** -> ***Edit this Instance***

- Edit the row "AcquireIPAddress" with the following details:

***Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/phpipam_next_ip

- Click ***Save*** to save the new Schema
