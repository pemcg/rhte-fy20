+++
title = "Register System in IPAM"
weight = 4
+++

### Add Ansible Playbook to register VM

In the previous chapter of this lab, an Ansible Playbook was used to get the next available IP Address from IPAM and use it during VM Provisioning. In this chapter an Ansible Playbook will be added to register the created Virtual Machine in IPAM.

### Create the Ansible Playbook Method

Starting with CloudForms 4.6 we can create Methods of type "Playbook" which, instead of running Ruby code, execute an Ansible Playbook.

- Click on the ***Methods*** Class and then the ***Methods*** tab the right part of the window

- Create a new Method to run an Ansible Playbook. Click on ***Configuration*** -> ***Add a new Method***

- Switch the Method Type to "Playbook"

- Use the following details to fill out the form.

***Name:*** phpipam_add_device

***Display Name:*** register the system in IPAM

***Repository:*** GitLab

***Playbook:*** playbooks/phpipam-add-device.yaml

***Machine Credentials:*** Virtual Machine Credentials

***Hosts:*** localhost

***Max TTL (mins):*** 30

***Logging Output:*** On Error

- Click ***Add*** to create the Ansible Playbook Method

### Create the Ansible Playbook Instance

To be able to call the Method from a StateMachine, we need an associated Instance.

- Click on the ***Instances*** tab and ***Configuration*** -> ***Add a new Instance***

- Enter the following details into the Dialog:

***Name:*** phpipam_add_device

***Display Name:*** register the system in IPAM

***Fields:*** In the table search the row "execute" and put "phpipam_add_device" into the "value" field

- Click ***Add*** to save the Instance

### Modify the State Machine

To run the Ansible Playbook during Virtual Machine Provisioning, we have to change the "ActivateCMDB" state to the State Machine.

- Click on the "Provision VM from Template" Instance

- Click on ***Configuration*** -> ***Edit this Instance***

- Edit the row "ActivateCMDB" with the following details:

***Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/phpipam_add_device

- Click ***Save*** to save the new Schema

{{% notice note %}}
We use an existing placeholder state for our new Ruby Method. It would be more clean to modify the State Machine, but for the purpose of this lab, we want to keep things simple.
{{% /notice %}}
