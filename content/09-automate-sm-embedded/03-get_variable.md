+++
title = "Get Variable from Playbook"
weight = 3
+++

### Get the next available IP Address from IPAM

A typical use case is to implement integration with external systems by utilizing Ansible. In this example we want to get the next available IP Address from PHPIPAM by using an Ansible Playbook. The Playbook has been configured in the previous chapter. This chapter will add a Ruby Method which will extract the IP Addresss and store it for later use.

### Create the Ruby Method

- Click on the ***Methods*** Class and then the ***Methods*** tab the right part of the window

- Create a new Method to run a Ruby Method. Click on ***Configuration*** -> ***Add a new Method***

- Switch the Method Type to "Inline"

- Use the following details to fill out the form.

***Name:*** get_variable

***Display Name:*** get variable from Ansible Playbook

Add the following code:

    $evm.log("info", "EVM Automate Method Started")

    # Dump all of root's attributes to the log
    $evm.root.attributes.sort.each { |k, v| $evm.log("info", "Root:<$evm.root> Attribute - #{k}: #{v}")}

    ipaddr=$evm.get_state_var("ansible_stats_ip_addr")
    $evm.log("info", "State vars: #{ipaddr}")

    prov=$evm.root["miq_provision"]

    prov.set_option(:ip_addr,ipaddr)

- Click ***Validate*** to verify there is no syntax error in the Ruby Code. The result of the check will be shown on the top of the page.

- Click ***Add*** to create save the Ruby Method

### Create the Instance

To be able to call the Method from a StateMachine, we need an associated Instance.

- Click on the ***Instances*** tab and ***Configuration*** -> ***Add a new Instance***

- Enter the following details into the Dialog:

***Name:*** get_variable

***Display Name:*** get variable from Ansible Playbook

***Fields:*** In the table search the row "execute" and put "get_variable" into the "value" field

- Click ***Add*** to save the Instance

### Modify the State Machine

To run the Ansible Playbook during Virtual Machine Provisioning, we have to change the "AcquireMACAddress" state to the State Machine.

- Click on the "Provision VM from Template" Instance

- Click on ***Configuration*** -> ***Edit this Instance***

- Edit the row "AcquireMACAddress" with the following details:

***Value:*** /Infrastructure/VM/Provisioning/StateMachines/Methods/get_variable

- Click ***Save*** to save the new Schema

{{% notice note %}}
We use an existing placeholder state for our new Ruby Method. It would be more clean to modify the State Machine, but for the purpose of this lab, we want to keep things simple.
{{% /notice %}}
