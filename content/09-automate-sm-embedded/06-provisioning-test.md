+++
title = "Provisioning Test"
weight = 6
+++

### Test the updated Virtual Machine Provisioning State Machine

We want to verify the applied changed by deploying a Virtual Machine and check if httpd was actually installed properly. This time we bypass the Service Catalog and provision a Virtual Machine directly.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

{{% notice note %}}
Make sure you're on the root "All VMs & Templates". If you see the details of a Virtual Machine or Template, the "Provision VMs" item will not appear in the "Lifecycle" menu.
{{% /notice %}}

- Click on ***Lifecycle*** -> ***Provision VMs***

- Select the "RHEL-7.6" template and click ***Continue***

- On the ***Requests*** tab the email address is the only required field. Enter your email address and optionally enter some data into the other fields as well

- Switch to the ***Catalog*** tab and specify a VM name

{{% notice warning %}}
Make sure to not use an existing name or provisioning will fail. You can use "changeme" and CloudForms will automatically assign a name to the Virtual Machine.
{{% /notice %}}

- Switch to the ***Environment*** tab and check the following fields:

***Choose Automatically:*** not selected

***Datacenter:*** Default

***Cluster:*** Default

***Host:*** rhvh1.example.com

***Datastore:*** data

- Switch to the ***Customize*** tab and check the following fields:

***Root Password:*** r3dh4t1!

***Customize:*** set to "Specification"

***Address Mode:*** Static

***IP Address:*** 192.168.0.66

{{% notice note %}}
The IP above will actually not be used since it's set by the phpIPAM systems, but the field has to be set and can not be empty.
{{% /notice %}}

***Subnet Mask:*** 24

{{% notice warning %}}
Make sure to use 24 and not 255.255.255.0 - cloud-init will fail otherwise.
{{% /notice %}}

***Gateway:*** 192.168.0.2

***DNS Server list:*** 192.168.0.1

In the section ***Customize Template*** and select "ovirt-cloud-init" in the "Script Name" list.

- Click ***Submit*** to place your request

- You will be redirected to the Requests page. You can use the ***Refresh*** button on the top of the page, since it does not automatically reload, to watch your request progressing.

### Verify the result

If you want to make sure everything works fine, please keep in mind that provisioning a VM in the lab environment will take some time. There is also an additional delay until the IP Address will show up in the VM properties.

In this lab environment, the Virtual Machine is not accessible directly from the internet. Choose one of the following options to verify the Playbook did execute

- Open a remote Console: Navigate to the Virtual Machine you just ordered and click on ***Access*** -> ***VM Access***

{{% notice note %}}
If you do this the first time, your Web Browser might block the Popup Window!
{{% /notice %}}

- Perform Smart State Analysis: Navigate to the Virtual Machine you just ordered and click on ***Configuration*** -> ***Perform SmartState Analysis***. The action can take a few minutes to complete. After it finished, you should see the list of installed packages and can verify httpd is on the list

- Check the log files: First you will have to SSH into the workstation and from there you can log into CloudForms. You should find the hostnames in the RHPDS details. The Ansible logs can be found in /var/lib/awx/job_status.
