+++
title = "Customizing the State Machine"
weight = 1
+++

In this part of the lab, you will perform a series of tasks to customize the Provisioning State Machine of CloudForms. To better understand the following steps a quick introduction and overview of the process might be useful.

### Customizing CloudForms

CloudForms provides two mechanisms for backend customization. Since CloudForms is written in Ruby we can use Ruby Methods for our changes. The awesome [Mastering Automation in CloudForms](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/) and [Mastering Automation in CloudForms Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) are excellent sources to learn everything about these capabilities.

Since Red Hat acquired [Ansible](www.ansible.com) the integration with CloudForms is constantly improved. We can use Ansible Playbooks and Ansible Tower Jobs and Workflows in the Service Catalog, in the Automate State machine and several other areas.

### What's a State Machine

A [State Machine](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/state_machines/chapter.html) describes a series of tasks which have to be executed. There are a number of State Machines as part of the product. For this lab, we will customize the [Provisioning State Machine](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/provisioning_a_virtual_machine/chapter.html).

### The use case

A very common integration tasks is to customize CloudForms to use an IP Address Management (IPAM) system provided by the customer. CloudForms is supposed to get the next free IP address, register the Virtual Machine and revert all these changes during [Retirement](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/vm_instance_retirement/chapter.html).

In the labe environment an instance of [phpIPAM](https://phpipam.net/) is deployed and can be used. The following chapters will guide you through the process of implementing the necessary changes.

1. [get IP from IPAM](../02-get-ip-from-playbook/): in the first state of the State Machine, CloudForms will use an Ansible Playbook to get the next free IP Address

1. [get Variable from Playbook](../03-get_variable/): a Ruby method is used to store the IP Address in the correct variable in the options hash

1. [register System in IPAM](../04-register-vm/): after the VM was created a Ruby Method is used to store the hostname and IP Address in the ansible_stats

1. [send Variable to Playbook](../05-send_variable/): an Ansible Playbook is used to register the new Virtual Machine in phpIPAM
