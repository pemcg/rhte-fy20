+++
title = "Create Service Catalog"
weight = 2
+++

To offer a Service Catalog Item to users, they have to be organized in Service Catalogs.

### Create an Ansible Service Catalog

This part of the lab will guide you through the process of creating a Service Catalog.

- The next step is to create a Service Catalog. First we have to navigate to ***Services*** -> ***Catalogs***.

- On this screen click on ***Catalogs*** on the left

- Click on ***Configuration*** and ***Add a New Catalog***

- Fill out name and description:

***Name:*** Ansible

A user friendly name of the Service Catalog. End users will see the different Service Catalogs by name.

***Description:*** Order Ansible Playbooks from a Service Catalog

Additional description about the Service Catalog. End users will see the description and it will help them to find the Service Catalog Items they are looking for.

- Click on ***Add*** to save the new Catalog
