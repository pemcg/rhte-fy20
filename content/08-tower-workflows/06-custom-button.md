+++
title = "Custom Button for Tower Workflows"
weight = 6
+++

Starting with CloudForms 5.0 Custom Buttons can also run Ansible Tower Workflows. Before it was only possible to run Ansible Tower Jobs.

### Add a Button Group

To add new button to the UI, we first need to create a Button Group. A Button Group is basically a new menu entry in the UI. Buttons and Button Groups can be assigned to several objects in CloudForms.

{{% notice note %}}
If you've done the previous chapter on [Custom Buttons](../../07-custom-buttons-tower/), the Button Group will already exist and you can skip this task.
{{% /notice %}}

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

- Click on ***Buttons*** in the accordion on the left

- Click on ***VM and Instance***

- Click on ***Configuration*** -> ***Add a new Button Group***

- Enter the following data into the form:

***Text:*** Ansible Tower Tools

The name of the Button Group, or menu, as shown in the UI.

***Hover Text:*** Additional tasks

A description text which will be shown when hovering the mouse over the Button Group.

***Icon:*** search for the wrench symbol in ***Font Awesome***

An icon for the Button Group.

- Click ***Add*** to create the button group

In the next section we will add a button to the group.

### Add a new Button to the Button Group

The previous step created a Button Group, or menu. Now we want add Buttons to the Group:

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

{{% notice note %}}
You should already be in this menu if you followed the previous steps
{{% /notice %}}

- Click on the "Tools" Button Group you created in the previous lab

- Click on ***Configuration*** -> ***Add a new Button***

- Make the following adjustments:

***Button Type:*** Default

***Text:*** Enable Cockpit

***Hover Text:*** Enable Cockpit and Harden Machine

Click on the Icon symbol and choose an Icon of your liking.

***Dialog:*** Cockpit Workflow

Leave the other settings on this page unmodified and switch to ***Advanced***.

Scroll down to the section "Object Details" and apply the following settings:

***System/Process:*** Request

***Message:*** Create

***Request:*** ansible_tower_job

Under "Attribute/Value Pairs" add the following rows:

- Row 1: job_template_name / Cockpit Workflow

{{% notice note %}}
It's important to use the correct spelling or the Job will fail.
{{% /notice %}}

{{% notice warning %}}
This feature was introduced in CloudForms 5.0. Currently there is no automatic adjustment of the "limit" parameter! The Workflow will run targeting the entire inventory configured in the Workflow. There is currently no mechanism to dynamically set a limit.
{{% /notice %}}
