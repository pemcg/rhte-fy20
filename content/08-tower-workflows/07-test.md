+++
title = "Test the Custom Button"
weight = 7
+++

We want to test the resulting customization and see how it works from a user point of view.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

- Click on the "cfme008" tile if not already selected

{{% notice warning %}}
Make sure cfme008 is up and running or the Playbook will fail.
{{% /notice %}}

- On the details page of "cfme008" note the new menu "Tools". Click to see the new button "Install Package"

- Click on ***Ansible Tower*** -> ***Enable Cockpit***

The previously created Service Dialog will open and ask for a "Service Name". This field is optional and you can just click on ***Submit***.

- Navigate to ***Automation*** -> ***Ansible Tower*** -> ***Jobs***

{{% notice note %}}
You might have to reload the screen to see the job results.
{{% /notice %}}

After the Job was completed, you should see a now row in the table. Click on it to see more details. In this release you will not see the Ansible Playbook Output though.

If you navigate back to the Virtual Machine you will see a new field "Custom Button Events". If you click on it, you will also see the Job you just executed.

{{% notice note %}}
Ansible is idempotent - this means you can run the same Playbook many times and Ansible detects if the desired state was already reached. In this lab, no changes are necessary, because the package httpd is already installed.
{{% /notice %}}
