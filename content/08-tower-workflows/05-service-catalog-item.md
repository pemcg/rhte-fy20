+++
title = "Service Catalog for Tower Workflows"
weight = 5
+++

Service Catalog Items are presented to the end user. A Service Catalog Item has a name, description and an optional long description where HTML formatting is supported. This allows to build user friendly Items including some eye candy or the capability to use links to reference to additional documentation.

A Service Catalog Item also provides the metadata used to describe the steps executed during an order process.

### Create a Service Catalog Item

In the following step we create a Service Catalog Item which will execute an Ansible Playbook.

- Navigate to ***Services*** -> ***Catalogs***

{{% notice note %}}
If you followed the instructions by the letter, you're already in this part of the UI.
{{% /notice %}}

- Navigate to ***Catalog Items*** in the accordion on the left

- Click on ***Configuration*** -> ***Add a New Catalog Item***

- Select ***Ansible Tower*** as Catalog Item Type

- Use the following parameters when defining the Service Catalog Item:

***Name:*** Setup Cockpit

The user friendly name of the Service Catalog Item. It will be presented to the end user.

***Description:*** Setup Cockpit and Harden Machine

Additional description about the Service Catalog Item to make it easier for the end user to find what they are looking for.

***Display in Catalog:*** Yes

You can hide Service Catalog Items from users by setting this to "No". For this lab we want to allow users to order the Service Catalog Item, so we set this to "Yes".

***Catalog:*** My Company/Ansible Tower

In which Service Catalog do you want the Service Catalog Item to show up?

***Dialog:*** Cockpit Workflow

The Service Dialog created in the previous chapter of this lab.

***Zone:*** Default Zone

Starting with CloudForms 5.0, Ansible Jobs can be scheduled to run in a specific Zone. This might be necessary to reflect network architecture and security needs. In this lab, we only have the "Default" Zone.

***Provider:*** Ansible Tower Automation Manager

There might be many git repositories, to better identify the correct Ansible Playbook, first select the Repository.

***Ansible Tower Template:*** Cockpit Workflow

The actual Playbook which will be executed when the Service Catalog Item is ordered. Note that there are two sections in the lest: One for regular Jobs and one for Workflows. The "Cockpit Workflow" workflow is at the bottom of the list.

{{% notice note %}}
All other fields can remain set to their respective defaults.
{{% /notice %}}

- Click ***Add*** to save all changes
