+++
title = "Create Service Catalog"
weight = 3
+++

To offer a Service Catalog Item to users, they have to be organized in Service Catalogs.

### Create an Ansible Service Dialog

If the Job Template in Tower is well specified, CloudForms can automatically generate a [Service Dialog](/99-glossary/02-list#service-dialog) from it. For this to work, the extra variables required to run an Ansible Tower Job have to be configured as part of the Job Template Definition. When done correctly, the variables will show up in the CloudForms UI as a property of the Ansible Tower Job Template.

- Navigate to ***Automation*** -> ***Ansible Tower*** -> ***Explorer***

- Click on ***Templates*** in the accordion on the left

- Click on the ***InstallPackage*** Job Template

- Note the variable "package_name" with its default value "httpd" in the "Variables" section of the UI

- Click on ***Configuration*** -> ***Create Service Dialog from this Template***

***Service Dialog Name:*** InstallPackageJob

- A notification will indicate that the dialog was created

{{% notice info %}}
You can verify the automatically generated Service Dialog by navigating to ***Automation*** -> ***Automate*** -> ***Customization***. Click on ***Service Dialogs*** in the accordion on the left and you should see the automatically generated one. In a later part of this lab, you will learn how to modify a Service Dialog to make it more user friendly.
{{% /notice %}}

### Create an Ansible Service Catalog

This part of the lab will guide you through the process of creating a Service Catalog.

- The next step is to create a Service Catalog. First we have to navigate to ***Services*** -> ***Catalogs***.

- On this screen click on ***Catalogs*** on the left

- Click on ***Configuration*** and ***Add a New Catalog***

- Fill out name and description:

***Name:*** Ansible Tower

A user friendly name of the Service Catalog. End users will see the different Service Catalogs by name.

***Description:*** Order Ansible Playbooks from a Service Catalog

Additional description about the Service Catalog. End users will see the description and it will help them to find the Service Catalog Items they are looking for.

- Click on ***Add*** to save the new Catalog
