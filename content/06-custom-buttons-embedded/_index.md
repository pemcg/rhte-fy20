+++
title = "Custom Buttons with Embedded Ansible"
weight = 6
chapter = true
+++

# Custom Buttons with Embedded Ansible

Learn some basics of CloudForms, how to build a Service Catalog and leverage the power of Ansible.
